# SpotifyWjs

[![Build Status](https://travis-ci.org/hdusantos/SpotifyWjs.svg?branch=develop)](https://travis-ci.org/hdusantos/SpotifyWjs)
[![Coverage Status](https://coveralls.io/repos/github/hdusantos/SpotifyWjs/badge.svg?branch=develop)](https://coveralls.io/github/hdusantos/SpotifyWjs?branch=develop)

A Wrapper [Spotify Web API](https://developer.spotify.com/web-api/)

## Browser Support

![Chrome](https://cloud.githubusercontent.com/assets/398893/3528328/23bc7bc4-078e-11e4-8752-ba2809bf5cce.png) | ![Firefox](https://cloud.githubusercontent.com/assets/398893/3528329/26283ab0-078e-11e4-84d4-db2cf1009953.png) | ![Opera](https://cloud.githubusercontent.com/assets/398893/3528330/27ec9fa8-078e-11e4-95cb-709fd11dac16.png) | ![Safari](https://cloud.githubusercontent.com/assets/398893/3528331/29df8618-078e-11e4-8e3e-ed8ac738693f.png) | ![IE](https://cloud.githubusercontent.com/assets/398893/3528325/20373e76-078e-11e4-8e3a-1cb86cf506f0.png) |
--- | --- | --- | --- | --- |
39+ ✔ | 42+ ✔ | 29+ ✔ | 10.1+ ✔ | Nope ✘ |

## Dependencies

- Only [fetch](https://fetch.spec.whatwg.org/) - ( Alternatives to fetch: [polyfill](https://github.com/github/fetch) to browser or [polyfill](https://github.com/bitinn/node-fetch) to Node.)


## Authors

* **Hudson dos Santos** - *Initial work* - [SpotifyWjs](https://github.com/hudson-s/SpotifyWjs)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

